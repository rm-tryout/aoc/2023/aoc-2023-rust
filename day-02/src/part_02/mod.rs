use std::collections::HashMap;

use crate::part_01::collect_cube_count_for_takes;

pub fn process_part_02(input:&str) -> usize {
  input
    .lines()
    .map(|l|{
      let mut min_cubes_needed:HashMap<String, usize> = HashMap::new();
      // let game = collect_cube_count_for_takes(l);

      collect_cube_count_for_takes(l)
      .into_iter()
      .for_each(|(color, count)|{
        let current = min_cubes_needed.entry(color.clone()).or_insert(count);

        if count > *current{
          min_cubes_needed.insert(color, count);
        }
      });

      min_cubes_needed
      .iter()
      .fold(1, |acc, (_col, count)|{ acc * count})

    })
    .sum()
}
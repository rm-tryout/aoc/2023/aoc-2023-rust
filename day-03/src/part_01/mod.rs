use std::{ops::Range, collections::{HashSet, HashMap}};


pub fn get_numbers_from_line(line: &str, y: usize) -> HashMap<(Range<usize>, usize), usize> {

  let mut digits:Vec<char> = vec![];
  let mut result: HashMap<(Range<usize>, usize), usize> = HashMap::new();

  line
  .chars()
  .enumerate()
  .for_each(|(x, c)|{
    if c.is_ascii_digit() {
      digits.push(c)
    }

    let mut x = x;
    
    if !c.is_ascii_digit() && digits.len() > 0 {
      result.insert(
        (x-digits.len()..x, y), 
        digits.iter().map(|c| c.to_string()).collect::<Vec<String>>().join("").parse::<usize>().unwrap(),
      );
      digits = vec![]
    }

    if c.is_ascii_digit() && x == line.len() - 1{
      x = x+1;
      result.insert(
        (x-digits.len()..x, y), 
        digits.iter().map(|c| c.to_string()).collect::<Vec<String>>().join("").parse::<usize>().unwrap(),
      );
      digits = vec![]
    }

    
  });

  result
}

pub fn get_symbols(line: &str, y: usize) -> HashSet<(usize, usize)> {
  line
  .chars()
  .enumerate()
  .filter_map(|(ind, c)|{
    if !c.is_ascii_digit() &&  c != '.' {
      Some((ind, y))
    } else {
      None
    }
  })
  .collect()
}

pub fn has_adjacent_symbol(num: &((Range<usize>, usize), usize), symbols: &HashSet<(usize, usize)>) -> bool{  
  symbols
  .iter()
  .any(|(x,y)|{
    let x_0 = if num.0.0.start <= 0 { 0 } else { num.0.0.start - 1};
    let x_1 = num.0.0.end;
    let y_0 = if num.0.1 <= 0 { 0 } else { num.0.1 - 1};
    let y_1 = num.0.1 + 1;

    let x_in_range = (x_0..=x_1).contains(x);
    let y_in_range = (y_0..=y_1).contains(y);

    x_in_range && y_in_range
  })
}

pub fn process_part_01(input: &str) -> usize{
  let numbers:HashMap<(Range<usize>, usize), usize> = input
  .lines()
  .enumerate()
  .flat_map(|(y, l)| get_numbers_from_line(l, y))
  .collect();

  let symbols: HashSet<(usize, usize)> = input
  .lines()
  .enumerate()
  .flat_map(|(y, l)|{
    get_symbols(l, y)
  })
  .collect();
  
  numbers
  .into_iter()
  .filter(|n| has_adjacent_symbol(n, &symbols) )
  .map(|n| n.1 )
  .sum()

}


#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn parse_line_easy(){
    let input = "467..114..";
    
    let result = get_numbers_from_line(input, 5);

    assert_eq!(
      result,
      vec![
        ((0..3, 5), 467),
        ((5..8, 5), 114)
      ].into_iter().collect()
    );
  }

  #[test]
  fn parse_line_number_at_end(){
    let input = "467..114..125";
    
    let result = get_numbers_from_line(input, 5);

    assert_eq!(
      result,
      vec![
        ((0..3, 5), 467),
        ((5..8, 5), 114),
        ((10..13, 5), 125),
      ].into_iter().collect()
    );
  }

  #[test]
  fn parse_line_with_symbol(){
    let input = "617*......";
    
    let result = get_numbers_from_line(input, 2);    

    assert_eq!(
      result,
      vec![
        ((0..3, 2), 617),
      ].into_iter().collect()
    );
  }


  #[test]
  fn get_two_symbols(){
    let input = "...$.*....";

    let result = get_symbols(input, 2);

    assert_eq!(result, vec![
      (3,2),(5,2)
    ].into_iter().collect())
  }

  #[test]
  fn part_01_works_with_example(){
    let input = "467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..";

    let result = process_part_01(input);

    assert_eq!(result, 4361)
  }
}
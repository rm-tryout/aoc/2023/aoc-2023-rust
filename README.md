# AOC 2022 - RUST

This is my attempt on getting to know Rust a little better with AOC 2022.

## Get Input
There is a little helper script called get_input.sh to download your input directly from aoc into day-xx/input.txt. In order to make this work you first have to create the following file *creds/cookie.txt* The content of the file should be the content of the cookie header you use in aoc (easiest to copy it out from the browsers dev envs network tab). Just run

```sh
./get_input.sh X YYYY
```

where X is the day you want to download and YYYY is the year (defaults to 2022)


use std::collections::HashMap;

pub fn get_id_from_line(l:&str) -> usize {
  l.split(":")
  .into_iter()
  .next()
  .unwrap()
  .split(" ")
  .last()
  .unwrap()
  .parse::<usize>()
  .unwrap()
}

pub fn collect_cube_count_for_takes(l:&str) -> Vec<(String,usize)>{
  l.split(":").last().unwrap()
  .split(";")
  .flat_map(|take| {
    take.split(",")
    .map(|cubes|{
      let mut spl = cubes.trim().split(" ").into_iter();
      (spl.next().unwrap().parse::<usize>().unwrap(), spl.next().unwrap())
    })
    
  })
  .map(|(count, color)| (color.to_string(),count))
  .collect()
}

pub fn game_is_possible(game: &Vec<(String,usize)>, limits: &HashMap<String, usize>) -> bool {
  !game.iter().find(|&(color, count)|{
    limits.get(color).unwrap() < count
  }).is_some()
}

pub fn process_part_01(input: &str) -> usize{
  let limits = vec![
      ("red".to_string(), 12 ),
      ("green".to_string(), 13),
      ("blue".to_string(), 14)
    ].into_iter().collect::<HashMap<String, usize>>();

  input
    .lines()
    .filter_map(|l|{
      let game = collect_cube_count_for_takes(l);
      if game_is_possible(&game, &limits) {
        Some(get_id_from_line(l))
      } else {
        None
      }
    })
    .sum()
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn parse_id_from_line_works(){
    let input = "Game 100: 4 red, 2 blue, 4 green; 2 green, 1 red, 1 blue; 3 green, 4 blue, 5 red; 18 red, 2 blue; 9 red, 5 green, 4 blue";

    assert_eq!(get_id_from_line(input), 100);
  }

  #[test]
  fn part_01(){
    let input = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

    assert_eq!(process_part_01(input), 8);
  }
}
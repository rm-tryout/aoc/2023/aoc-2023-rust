use std::collections::HashSet;

pub fn get_cards_from_line(line: &str) -> Vec<HashSet<usize>>{
  let mut current_digit_list:Vec<char> = vec![];
  let mut set: HashSet<usize> = HashSet::new();
  let mut card_sets: Vec<HashSet<usize>> = vec![];

  let mut line_iter = line.split(":");

  let eol_index =  line.len() - line_iter.next().unwrap().len() - 2;

  line_iter
  .next()
  .unwrap()
  .chars()
  .enumerate()
  .for_each(|(ind, c)|{
    if c.is_ascii_digit(){
      current_digit_list.push(c);
    }

    if current_digit_list.len() > 0 && (ind == eol_index || !c.is_ascii_digit()) {
      set.insert(
        (&current_digit_list)
        .into_iter()
        .map(|c|c.to_string())
        .collect::<Vec<String>>()
        .join("")
        .parse::<usize>()
        .unwrap()
      );
      current_digit_list = vec![];
    }

    if c == '|' || ind == eol_index  {
      card_sets.push(set.clone());
      set = HashSet::new();
    }
  });

  card_sets

}

pub fn calc_points(card_sets: &Vec<HashSet<usize>>) -> usize {
  let c_1 = &card_sets[0];
  let c_2 = &card_sets[1];

  let match_count = c_1
  .into_iter()
  .filter(|winning_num| c_2.contains(winning_num) )
  .count();

  if match_count == 0 {
    0
  } else {
    i32::pow(2, (match_count-1) as u32) as usize
  }

}

pub fn process_part_01(input: &str) -> usize {
  input
  .lines()
  .map(|l|get_cards_from_line(l))
  .map(|card_sets| calc_points(&card_sets))
  .sum()
}


#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn get_cards_from_line_01(){
    let input = "Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53";
    let card_1:HashSet<usize> = (vec![41,48,83,86,17]).into_iter().collect();
    let card_2:HashSet<usize> = (vec![83,86,6,31,17,9,48,53]).into_iter().collect();


    assert_eq!(
      get_cards_from_line(input),
      vec![ card_1, card_2 ]
    )
  }

  #[test]
  fn calc_points_01(){
    let card_1:HashSet<usize> = (vec![41,48,83,86,17]).into_iter().collect();
    let card_2:HashSet<usize> = (vec![83,86,6,31,17,9,48,53]).into_iter().collect();

    assert_eq!(
      calc_points(&vec![card_1, card_2]),
      8
    )
  }
}
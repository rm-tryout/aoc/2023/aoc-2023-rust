use std::{collections::{HashMap, HashSet}, ops::Range};

use crate::part_01::get_numbers_from_line;

fn find_adjacent_numbers(numbers: &HashMap<(Range<usize>, usize), usize>, symbol: &(usize, usize)) -> HashMap<(Range<usize>, usize), usize>{
  numbers
  .to_owned()
  .into_iter()
  .filter_map(|num|{
    let x_0 = if num.0.0.start <= 0 { 0 } else { num.0.0.start - 1};
    let x_1 = num.0.0.end;
    let y_0 = if num.0.1 <= 0 { 0 } else { num.0.1 - 1};
    let y_1 = num.0.1 + 1;

    let x_in_range = (x_0..=x_1).contains(&symbol.0);
    let y_in_range = (y_0..=y_1).contains(&symbol.1);

    if x_in_range && y_in_range {
      Some(num)
    } else {
      None
    }
  })
  .collect()
}

pub fn get_gears(line: &str, y: usize) -> HashSet<(usize, usize)> {
  line
  .chars()
  .enumerate()
  .filter_map(|(ind, c)|{
    if c == '*' {
      Some((ind, y))
    } else {
      None
    }
  })
  .collect()
}

pub fn process_part_02(input:&str) -> usize {
  let numbers:HashMap<(Range<usize>, usize), usize> = input
  .lines()
  .enumerate()
  .flat_map(|(y, l)| get_numbers_from_line(l, y))
  .collect();

  input
  .lines()
  .enumerate()
  .flat_map(|(y, l)| get_gears(l, y))
  .filter_map(|g|{
    let adjacent_nums = find_adjacent_numbers(&numbers, &g);
    if adjacent_nums.len() == 2 {
      Some(
        adjacent_nums.iter().fold(1 as usize, |acc, curr| acc*curr.1 )
      )
    } else {
      None
    }
  })
  .sum()
}
pub fn process_part_02(input:&str) -> usize {
  let digits = vec![
    "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
    "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine"
  ];

  input
  .lines()
  .into_iter()
  .map(|l|{
    let mut found_digits = digits
    .iter()
    .enumerate()
    .map(|(index, &digit)| 
      (index % 10, l.find(digit) )
    )
    .filter(|(_, digit_position)| digit_position.is_some())
    .map(|(digit, position)| (position.unwrap(), digit) )
    .collect::<Vec<(usize, usize)>>();

    found_digits.sort_by(|(pos_1, _), (pos_2, _)|{ pos_1.cmp(pos_2) });

    format!("{}{}", found_digits.first().unwrap().1, found_digits.last().unwrap().1).parse::<usize>().unwrap()
    
  })
  .sum()
}


#[cfg(test)]
mod test {
use super::*;

  #[test]
  fn works_with_line(){
    let result = process_part_02("two1nine");
    assert_eq!(result, 29);
  }

  #[test]
  fn works_with_line_2(){
    let result = process_part_02("7pqrstsixteen");
    assert_eq!(result, 76);
  }

  #[test]
  fn works_with_line_3(){
    let result = process_part_02("abcone2threexyz");
    assert_eq!(result, 13);
  }

  #[test]
  fn works_with_line_4(){
    let result = process_part_02("xtwone3four");
    assert_eq!(result, 24);
  }

  #[test]
  fn works_with_line_5(){
    let result = process_part_02("4nineeightseven2");
    assert_eq!(result, 42);
  }

  #[test]
  fn works_with_line_6(){
    let result = process_part_02("zoneight234");
    assert_eq!(result, 14);
  }

  #[test]
  fn works_with_old_input(){
    let input = "1abc2
    pqr3stu8vwx
    a1b2c3d4e5f
    treb7uchet";

    let result = process_part_02(input);
    assert_eq!(result, 142);
  }

  #[test]
  fn works_with_complete_test(){
    let input = "two1nine
    eightwothree
    abcone2threexyz
    xtwone3four
    4nineeightseven2
    zoneight234
    7pqrstsixteen";

    let result = process_part_02(input);

    assert_eq!(result,281)
  }
}
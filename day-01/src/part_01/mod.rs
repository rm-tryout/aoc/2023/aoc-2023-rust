pub fn process_part_01(input: &str) -> i32{
  input
  .lines()
  .into_iter()
  .map(|l|{
    let mut l_forward = l.clone().chars();
    let mut l_backwards = l.clone().chars().rev();

    let c1 = l_forward.find(|c| c.is_ascii_digit()).unwrap();
    let c2 = l_backwards.find(|c|{c.is_ascii_digit()}).unwrap();

    format!("{}{}",c1,c2).parse::<i32>().unwrap()
  })
  .sum()
}